#include <Rcpp.h>
using namespace Rcpp;


// [[Rcpp::export]]
double simMarkovCpp(int ntrials,double delta){
   // initial point
  double x=1;
  double y=1;
  
  double delx;
  double dely;
  
 // initialize number of hits
 int nhits=0;

 for(int i=0; i<ntrials; ++i){
    //choose step
    delx=runif(1,-delta,delta)[0];
    dely=runif(1,-delta,delta)[0];
    if((abs(x+delx)<1.0) & (abs(y+dely)<1.0)){
      x=x+delx;
      y=y+dely;
    }
    if(pow(x,2)+pow(y,2)<1.0){
      nhits=nhits+1;
    }
     }
 return nhits;
    }        
           
