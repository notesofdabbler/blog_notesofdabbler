```{r chunkoptions, echo=FALSE}
opts_chunk$set(tidy=FALSE,cache=FALSE)
```

# Peter Norvig's Spelling Corrector Introduction: Attempt at R version

I came across the following [tutorial](http://nbviewer.ipython.org/url/norvig.com/ipython/How%20to%20Do%20Things%20with%20Words.ipynb) introduction to some elements of natural language processing by Peter Norvig. It is very well written and in ipython notebook that enables working through the tutorial. I thought it will be a good exercise for me to see if I can do the first part of the tutorial (correcting spelling) in R. Below is my attempt at doing it. I will put the disclaimer that my code will not come remotely close to Peter Norvig's code on speed and elegance. Given some internet traffic on R vs Python discussion, I will point out that this reflects my coding ability than ability of R. Folks who are passionate about R are welcome to come up with faster and more elegant versions of the code.:-). 

## Training Data

Peter Norvig uses text file with about million words that is a combination of several public domain textbooks. That is in the file [big.txt](http://norvig.com/big.txt). He uses the frequency of occurence of words in this text for the spell corrector. 

```{r loadbigtxt}
# load libraries
library(dplyr)
library(stringr)

# set working directory
setwd("~/notesofdabbler/blog_notesofdabbler/norvigSpellCheck/")

#-------get the collection of words from the file big.txt----------------------

# read text as a single string
BIG=paste(readLines("big.txt"),collapse=" ")
# Size of file
object.size(BIG)
# split into words
wsplit=strsplit(tolower(BIG)," ")
ptm=proc.time()
wmatch=str_extract_all(wsplit[[1]],"[a-z]+")
proc.time()-ptm
wordlist=data.frame(wordlist=unlist(wmatch),stringsAsFactors=FALSE)

# create a dataframe with count of words in big.txt
WORDS=wordlist%>%group_by(wordlist)%>%summarise(count=n())
row.names(WORDS)=WORDS$wordlist
head(WORDS)

```

I seem to be using an inefficient way to do this as seen from the time. I tried the following alternative which also took long. It will be interesting to know a better way to accomplish this.

```{r loadbigtxtnw, eval=FALSE}
wmatch=gregexpr("[a-z]+",tolower(text))
wordlist=regmatches(tolower(text),wmatch)
```

Check number of unique words, total number of words and top 20 commonly used words in big.txt
```{r explorebigtxt}
# list top 20 common words
top20words=arrange(WORDS,desc(count))%>%head(20)
top20words

# check number of unique words and total number of words
nrow(WORDS)
sum(WORDS$count)
```

## Spell Corrector 

I have tried to capture Peter Norvig's logic in a simplified manner (see his tutorial for details and also underpinnings to a probability model):

1. Find all words at 0 edit distance from given word (which will be the word itself). If it is a known word (i.e it appears in big.txt) then stop else go to step 2.
2. Find all words at 1 edit distance from given word. If there are known words in this list, pick the word with highest frequency and stop else go to step 2.
3. Find all words at 2 edit distance from given word. If there are known words in this list, pick the word with highest frequency and stop else the status is unknown.

Below is the function to get 0 edit distance
```{r edit0}
#------- find all zero edits of the given word: this will return the same word------
edits0=function(word){
  return(word)
}
```

The 1 edit distance of a given word is computed by doing the following (illustrated with word 'wird'):
* Finding all deletions of a single character
```{r deletions}
# Example using the word 'wird'
word='wird'

# find different splits of a word
splits=function(word){
    splits=lapply(0:nchar(word),function(y) list(lw=substr(word,1,y),rw=substr(word,y+1,nchar(word))))
    return(splits)
}

pairs=splits(word)
unlist(lapply(pairs,function(x) paste('(',x$lw,',',x$rw,')',paste="")))

# find all deletes of the word
pairslist1=sapply(pairs,function(x) x$rw != "")
deletes=lapply(pairs[pairslist1],function(x) paste(x$lw,substr(x$rw,2,nchar(x$rw)),sep=""))
deletes=unlist(deletes)
deletes
```

* Finding all transposes of two adjacent characters
```{r transposes}
# find all transposes of the word
pairslist2=sapply(pairs,function(x) nchar(x$rw)>1)
transposes=lapply(pairs[pairslist2],
                  function(x) paste(x$lw,substr(x$rw,2,2),substr(x$rw,1,1),substr(x$rw,3,nchar(x$rw)),sep=""))
transposes=unlist(transposes)
transposes
```

* Finding all replacements of one character by any letter of alphabet
```{r replaces}
replaces=lapply(pairs[pairslist1],
               function(x) lapply(letters,
                                  function(y) paste(x$lw,y,substr(x$rw,2,nchar(x$rw)),sep="")))
replaces=unlist(replaces)
replaces
```

* Finding all insertions using any letter of the alphabet
```{r insertions}
inserts=lapply(pairs,
               function(x) lapply(letters,
                                  function(y) paste(x$lw,y,x$rw,sep="")))
inserts=unlist(inserts)
inserts
```

All of the above is captured in the function below for finding 1 edit distances.
```{r edit1fn}
edits1=function(word){
  pairs=splits(word)
  pairslist1=sapply(pairs,function(x) x$rw != "")
  deletes=lapply(pairs[pairslist1],function(x) paste(x$lw,substr(x$rw,2,nchar(x$rw)),sep=""))
  deletes=unlist(deletes)
  
  pairslist2=sapply(pairs,function(x) nchar(x$rw)>1)
  transposes=lapply(pairs[pairslist2],
                    function(x) paste(x$lw,substr(x$rw,2,2),substr(x$rw,1,1),substr(x$rw,3,nchar(x$rw)),sep=""))
  transposes=unlist(transposes)
  
  replaces=lapply(pairs[pairslist1],
                  function(x) lapply(letters,
                                     function(y) paste(x$lw,y,substr(x$rw,2,nchar(x$rw)),sep="")))
  replaces=unlist(replaces)
  
  inserts=lapply(pairs,
                 function(x) lapply(letters,
                                    function(y) paste(x$lw,y,x$rw,sep="")))
  inserts=unlist(inserts)
  
  edits1=unique(c(deletes,transposes,replaces,inserts))

  return(edits1)
}
```

The 2 edit distance words are determined by finding 1 edit distance o words that are 1 edit distance from a given word.
```{r edits2fn}
edits2=function(word){
  edits1=edits1(word)
  edits2=lapply(edits1,function(x) edits1(x))
  edits2=unique(unlist(edits2))
  return(edits2)
}
# the number of edit 2 distance words are high
# example with 'wird'
length(edits2('wird'))
# finding edit2 distances also takes time which adds up when checking across multiple words
system.time(edits2('wird'))
```
This step adds when checking spelling across multiple words.

The following function checks the subset of candidate words that are known.
```{r knownfn}
# check if a set of words are known (in the master WORDS list)
known=function(words){
  known=filter(WORDS,wordlist %in% words)
  return(known)
}
```

The following function is used to determine the correct spelling
```{r correctspell}
# find the correct word using the following order
# choose the max frequency word with edit distance 0 from the given word. 
# If no distance 0 word is known, choose the max frequency word with edit distance 1 from the given word
# If no distance 1 word is known, choose the max frequency word with edit distance 2 from the given word

correct=function(word){
  # known words with edit distance 0 from given word
  candidates0=known(edits0(word))
  # known words with edit distance 1 from given word
  candidates1=known(edits1(word))
  # known words with edit distance 2 from given word
  candidates2=known(edits2(word))
  
  if(nrow(candidates0) > 0){
    correct=candidates0%>%filter(count==max(count))%>%select(wordlist)
    return(correct[1,1])
  } else if (nrow(candidates1) >0){
    correct=candidates1%>%filter(count==max(count))%>%select(wordlist)
    return(correct[1,1])
  } else if (nrow(candidates2) > 0){
    correct=candidates2%>%filter(count==max(count))%>%select(wordlist)
    return(correct[1,1])
  } else {
    return(word)
  }
}
```

Check some spellings
```{r checkspell}
# check some spellings
correct("ello")
correct("accesing")
correct("msitake")
```
As you can see, this algorithm does not always get it right. Peter Norvig did a test with a set of 270 words with erros which is shown later. He also had an example of using this to correct a sentence. The idea here is to output the result also in the form of a sentence and keep the original case of the word where possible (upper, lower or title case)(Note: I couldn't locate a title case function in R but it probably exists. Here I created my own)

```{r sentencechk}
#---example of working with a sentence--------------------------------------------------

# custom function to check title case: ex: 'Hello','Yellow'
titlecase=function(word){
  if(nchar(word)>1){
    titlecasestr=paste(toupper(substr(word,1,1)),tolower(substr(word,2,nchar(word))),sep="")
  } else {
    titlecasestr=toupper(word)
  }
  
  return(titlecasestr)
}

# function: upper, lower or title case to apply to a word
case_of=function(word){
  if(word == toupper(word)){
    return(toupper)
  } else if (word == tolower(word)){
    return(tolower)
  } else if (word == titlecase(word)){
    return(titlecase)
  } else {
    return(identity)
  }
}

# text that needs correction
txt='Speling Errurs IN somethink. Whutever; unusuel misteakes?'
txt2=str_extract_all(txt,"[a-zA-Z]+")
txt2
# apply correction to each word
txt3=lapply(txt2[[1]],function(x) case_of(x)(correct(tolower(x))))
txt3

# get the correction in the same format or original text sentence
txt4=txt
for(i in 1:length(txt2[[1]])){
  x=txt2[[1]][i]
  txt4=gsub(x,case_of(x)(correct(tolower(x))),txt4)
}
txt4
```

## Test of Spell Corrector

Peter Norvig used a set of words with errors for testing. They are in the file [tests1.json][tests1json]. I tried using fromJSON from RJSONIO package but somehow couldn't get it to work. So for the time being I worked with the following file [tests1.txt][tests1txt] (same as json file but with quotes removed) and the code below.

```{r loadtest}
#-----Test based on 270 words with errors-----------------------------------------

# read the list of words with errors: they are in the format 
# target_word: wrong_words (there are cases with greater than 1 possible ways of writing wrong words)
#
tmp=paste(readLines("tests1.txt"),collapse="")
# split the list of (target,wrong) pairs
tmp2=strsplit(tmp,",")
# split each (target,wrong) pair based on : separator
tmp3=lapply(tmp2[[1]],function(x) str_trim(str_split(x,":")[[1]]))
# split the list of wrong words using space separator
tmp4=lapply(tmp3,function(x) list(target=x[1],
                                  wrongs=str_split(x[2]," ")[[1]]))

tmp4[1:5]
```

This data is used to find percentage of correct instances of spelling correction.

```{r runtest}
# count total number of words and number of wrong words
ptm <- proc.time()
bad=0
tot=0
for (i in 1:length(tmp4)){
#  print(i)
  nw=length(tmp4[[i]]$wrongs)
  for (j in 1:nw){
      tot=tot+1
      correctword=correct(tmp4[[i]]$wrongs[j])
      if (correctword != tmp4[[i]]$target) {bad=bad+1}
    }
}

# percentage correct
pctcorrect=1-bad/tot
pctcorrect

proc.time()-ptm
```
About 74% of words are correctly corrected for spelling.

## Session Information

This code was written in RStudio (0.98.507).

```{r sessioninfo}
sessionInfo()
```
[tests1json]:https://bitbucket.org/notesofdabbler/blog_notesofdabbler/src/master/norvigSpellCheck/tests1.json
[tests1txt]:https://bitbucket.org/notesofdabbler/blog_notesofdabbler/src/master/norvigSpellCheck/tests1.txt