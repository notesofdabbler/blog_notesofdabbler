


# Peter Norvig's Spelling Corrector Introduction: Attempt at R version

I came across the following [tutorial](http://nbviewer.ipython.org/url/norvig.com/ipython/How%20to%20Do%20Things%20with%20Words.ipynb) introduction to some elements of natural language processing by Peter Norvig. It is very well written and in ipython notebook that enables working through the tutorial. I thought it will be a good exercise for me to see if I can do the first part of the tutorial (correcting spelling) in R. Below is my attempt at doing it. I will put the disclaimer that my code will not come remotely close to Peter Norvig's code on speed and elegance. Given some internet traffic on R vs Python discussion, I will point out that this reflects my coding ability than ability of R. Folks who are passionate about R are welcome to come up with faster and more elegant versions of the code.:-). 

## Training Data

Peter Norvig uses text file with about million words that is a combination of several public domain textbooks. That is in the file [big.txt](http://norvig.com/big.txt). He uses the frequency of occurence of words in this text for the spell corrector. 


```r
# load libraries
library(dplyr)
```

```
## Warning: package 'dplyr' was built under R version 3.0.3
```

```
## 
## Attaching package: 'dplyr'
## 
## The following objects are masked from 'package:stats':
## 
##     filter, lag
## 
## The following objects are masked from 'package:base':
## 
##     intersect, setdiff, setequal, union
```

```r
library(stringr)

# set working directory
setwd("~/notesofdabbler/blog_notesofdabbler/norvigSpellCheck/")

#-------get the collection of words from the file big.txt----------------------

# read text as a single string
BIG=paste(readLines("big.txt"),collapse=" ")
# Size of file
object.size(BIG)
```

```
## 6488760 bytes
```

```r
# split into words
wsplit=strsplit(tolower(BIG)," ")
ptm=proc.time()
wmatch=str_extract_all(wsplit[[1]],"[a-z]+")
proc.time()-ptm
```

```
##    user  system elapsed 
##   95.35    0.42   95.77
```

```r
wordlist=data.frame(wordlist=unlist(wmatch),stringsAsFactors=FALSE)

# create a dataframe with count of words in big.txt
WORDS=wordlist%>%group_by(wordlist)%>%summarise(count=n())
row.names(WORDS)=WORDS$wordlist
head(WORDS)
```

```
## Source: local data frame [6 x 2]
## 
##        wordlist count
## a             a 21155
## aah         aah     1
## aaron     aaron     5
## ab           ab     2
## aback     aback     3
## abacus   abacus     1
```


I seem to be using an inefficient way to do this as seen from the time. I tried the following alternative which also took long. It will be interesting to know a better way to accomplish this.


```r
wmatch=gregexpr("[a-z]+",tolower(text))
wordlist=regmatches(tolower(text),wmatch)
```


Check number of unique words, total number of words and top 20 commonly used words in big.txt

```r
# list top 20 common words
top20words=arrange(WORDS,desc(count))%>%head(20)
top20words
```

```
## Source: local data frame [20 x 2]
## 
##    wordlist count
## 1       the 80030
## 2        of 40025
## 3       and 38313
## 4        to 28766
## 5        in 22050
## 6         a 21155
## 7      that 12512
## 8        he 12401
## 9       was 11410
## 10       it 10681
## 11      his 10034
## 12       is  9774
## 13     with  9740
## 14       as  8064
## 15        i  7687
## 16      had  7383
## 17      for  6941
## 18       at  6791
## 19       by  6738
## 20       on  6643
```

```r

# check number of unique words and total number of words
nrow(WORDS)
```

```
## [1] 29157
```

```r
sum(WORDS$count)
```

```
## [1] 1105285
```


## Spell Corrector 

I have tried to capture Peter Norvig's logic in a simplified manner (see his tutorial for details and also underpinnings to a probability model):

1. Find all words at 0 edit distance from given word (which will be the word itself). If it is a known word (i.e it appears in big.txt) then stop else go to step 2.
2. Find all words at 1 edit distance from given word. If there are known words in this list, pick the word with highest frequency and stop else go to step 2.
3. Find all words at 2 edit distance from given word. If there are known words in this list, pick the word with highest frequency and stop else the status is unknown.

Below is the function to get 0 edit distance

```r
#------- find all zero edits of the given word: this will return the same word------
edits0=function(word){
  return(word)
}
```


The 1 edit distance of a given word is computed by doing the following (illustrated with word 'wird'):
* Finding all deletions of a single character

```r
# Example using the word 'wird'
word='wird'

# find different splits of a word
splits=function(word){
    splits=lapply(0:nchar(word),function(y) list(lw=substr(word,1,y),rw=substr(word,y+1,nchar(word))))
    return(splits)
}

pairs=splits(word)
unlist(lapply(pairs,function(x) paste('(',x$lw,',',x$rw,')',paste="")))
```

```
## [1] "(  , wird ) " "( w , ird ) " "( wi , rd ) " "( wir , d ) "
## [5] "( wird ,  ) "
```

```r

# find all deletes of the word
pairslist1=sapply(pairs,function(x) x$rw != "")
deletes=lapply(pairs[pairslist1],function(x) paste(x$lw,substr(x$rw,2,nchar(x$rw)),sep=""))
deletes=unlist(deletes)
deletes
```

```
## [1] "ird" "wrd" "wid" "wir"
```


* Finding all transposes of two adjacent characters

```r
# find all transposes of the word
pairslist2=sapply(pairs,function(x) nchar(x$rw)>1)
transposes=lapply(pairs[pairslist2],
                  function(x) paste(x$lw,substr(x$rw,2,2),substr(x$rw,1,1),substr(x$rw,3,nchar(x$rw)),sep=""))
transposes=unlist(transposes)
transposes
```

```
## [1] "iwrd" "wrid" "widr"
```


* Finding all replacements of one character by any letter of alphabet

```r
replaces=lapply(pairs[pairslist1],
               function(x) lapply(letters,
                                  function(y) paste(x$lw,y,substr(x$rw,2,nchar(x$rw)),sep="")))
replaces=unlist(replaces)
replaces
```

```
##   [1] "aird" "bird" "cird" "dird" "eird" "fird" "gird" "hird" "iird" "jird"
##  [11] "kird" "lird" "mird" "nird" "oird" "pird" "qird" "rird" "sird" "tird"
##  [21] "uird" "vird" "wird" "xird" "yird" "zird" "ward" "wbrd" "wcrd" "wdrd"
##  [31] "werd" "wfrd" "wgrd" "whrd" "wird" "wjrd" "wkrd" "wlrd" "wmrd" "wnrd"
##  [41] "word" "wprd" "wqrd" "wrrd" "wsrd" "wtrd" "wurd" "wvrd" "wwrd" "wxrd"
##  [51] "wyrd" "wzrd" "wiad" "wibd" "wicd" "widd" "wied" "wifd" "wigd" "wihd"
##  [61] "wiid" "wijd" "wikd" "wild" "wimd" "wind" "wiod" "wipd" "wiqd" "wird"
##  [71] "wisd" "witd" "wiud" "wivd" "wiwd" "wixd" "wiyd" "wizd" "wira" "wirb"
##  [81] "wirc" "wird" "wire" "wirf" "wirg" "wirh" "wiri" "wirj" "wirk" "wirl"
##  [91] "wirm" "wirn" "wiro" "wirp" "wirq" "wirr" "wirs" "wirt" "wiru" "wirv"
## [101] "wirw" "wirx" "wiry" "wirz"
```


* Finding all insertions using any letter of the alphabet

```r
inserts=lapply(pairs,
               function(x) lapply(letters,
                                  function(y) paste(x$lw,y,x$rw,sep="")))
inserts=unlist(inserts)
inserts
```

```
##   [1] "awird" "bwird" "cwird" "dwird" "ewird" "fwird" "gwird" "hwird"
##   [9] "iwird" "jwird" "kwird" "lwird" "mwird" "nwird" "owird" "pwird"
##  [17] "qwird" "rwird" "swird" "twird" "uwird" "vwird" "wwird" "xwird"
##  [25] "ywird" "zwird" "waird" "wbird" "wcird" "wdird" "weird" "wfird"
##  [33] "wgird" "whird" "wiird" "wjird" "wkird" "wlird" "wmird" "wnird"
##  [41] "woird" "wpird" "wqird" "wrird" "wsird" "wtird" "wuird" "wvird"
##  [49] "wwird" "wxird" "wyird" "wzird" "wiard" "wibrd" "wicrd" "widrd"
##  [57] "wierd" "wifrd" "wigrd" "wihrd" "wiird" "wijrd" "wikrd" "wilrd"
##  [65] "wimrd" "winrd" "wiord" "wiprd" "wiqrd" "wirrd" "wisrd" "witrd"
##  [73] "wiurd" "wivrd" "wiwrd" "wixrd" "wiyrd" "wizrd" "wirad" "wirbd"
##  [81] "wircd" "wirdd" "wired" "wirfd" "wirgd" "wirhd" "wirid" "wirjd"
##  [89] "wirkd" "wirld" "wirmd" "wirnd" "wirod" "wirpd" "wirqd" "wirrd"
##  [97] "wirsd" "wirtd" "wirud" "wirvd" "wirwd" "wirxd" "wiryd" "wirzd"
## [105] "wirda" "wirdb" "wirdc" "wirdd" "wirde" "wirdf" "wirdg" "wirdh"
## [113] "wirdi" "wirdj" "wirdk" "wirdl" "wirdm" "wirdn" "wirdo" "wirdp"
## [121] "wirdq" "wirdr" "wirds" "wirdt" "wirdu" "wirdv" "wirdw" "wirdx"
## [129] "wirdy" "wirdz"
```


All of the above is captured in the function below for finding 1 edit distances.

```r
edits1=function(word){
  pairs=splits(word)
  pairslist1=sapply(pairs,function(x) x$rw != "")
  deletes=lapply(pairs[pairslist1],function(x) paste(x$lw,substr(x$rw,2,nchar(x$rw)),sep=""))
  deletes=unlist(deletes)
  
  pairslist2=sapply(pairs,function(x) nchar(x$rw)>1)
  transposes=lapply(pairs[pairslist2],
                    function(x) paste(x$lw,substr(x$rw,2,2),substr(x$rw,1,1),substr(x$rw,3,nchar(x$rw)),sep=""))
  transposes=unlist(transposes)
  
  replaces=lapply(pairs[pairslist1],
                  function(x) lapply(letters,
                                     function(y) paste(x$lw,y,substr(x$rw,2,nchar(x$rw)),sep="")))
  replaces=unlist(replaces)
  
  inserts=lapply(pairs,
                 function(x) lapply(letters,
                                    function(y) paste(x$lw,y,x$rw,sep="")))
  inserts=unlist(inserts)
  
  edits1=unique(c(deletes,transposes,replaces,inserts))

  return(edits1)
}
```


The 2 edit distance words are determined by finding 1 edit distance o words that are 1 edit distance from a given word.

```r
edits2=function(word){
  edits1=edits1(word)
  edits2=lapply(edits1,function(x) edits1(x))
  edits2=unique(unlist(edits2))
  return(edits2)
}
# the number of edit 2 distance words are high
# example with 'wird'
length(edits2('wird'))
```

```
## [1] 24254
```

```r
# finding edit2 distances also takes time which adds up when checking across multiple words
system.time(edits2('wird'))
```

```
##    user  system elapsed 
##    0.45    0.00    0.45
```

This step adds when checking spelling across multiple words.

The following function checks the subset of candidate words that are known.

```r
# check if a set of words are known (in the master WORDS list)
known=function(words){
  known=filter(WORDS,wordlist %in% words)
  return(known)
}
```


The following function is used to determine the correct spelling

```r
# find the correct word using the following order
# choose the max frequency word with edit distance 0 from the given word. 
# If no distance 0 word is known, choose the max frequency word with edit distance 1 from the given word
# If no distance 1 word is known, choose the max frequency word with edit distance 2 from the given word

correct=function(word){
  # known words with edit distance 0 from given word
  candidates0=known(edits0(word))
  # known words with edit distance 1 from given word
  candidates1=known(edits1(word))
  # known words with edit distance 2 from given word
  candidates2=known(edits2(word))
  
  if(nrow(candidates0) > 0){
    correct=candidates0%>%filter(count==max(count))%>%select(wordlist)
    return(correct[1,1])
  } else if (nrow(candidates1) >0){
    correct=candidates1%>%filter(count==max(count))%>%select(wordlist)
    return(correct[1,1])
  } else if (nrow(candidates2) > 0){
    correct=candidates2%>%filter(count==max(count))%>%select(wordlist)
    return(correct[1,1])
  } else {
    return(word)
  }
}
```


Check some spellings

```r
# check some spellings
correct("ello")
```

```
## [1] "hello"
```

```r
correct("accesing")
```

```
## [1] "acceding"
```

```r
correct("msitake")
```

```
## [1] "mistake"
```

As you can see, this algorithm does not always get it right. Peter Norvig did a test with a set of 270 words with erros which is shown later. He also had an example of using this to correct a sentence. The idea here is to output the result also in the form of a sentence and keep the original case of the word where possible (upper, lower or title case)(Note: I couldn't locate a title case function in R but it probably exists. Here I created my own)


```r
#---example of working with a sentence--------------------------------------------------

# custom function to check title case: ex: 'Hello','Yellow'
titlecase=function(word){
  if(nchar(word)>1){
    titlecasestr=paste(toupper(substr(word,1,1)),tolower(substr(word,2,nchar(word))),sep="")
  } else {
    titlecasestr=toupper(word)
  }
  
  return(titlecasestr)
}

# function: upper, lower or title case to apply to a word
case_of=function(word){
  if(word == toupper(word)){
    return(toupper)
  } else if (word == tolower(word)){
    return(tolower)
  } else if (word == titlecase(word)){
    return(titlecase)
  } else {
    return(identity)
  }
}

# text that needs correction
txt='Speling Errurs IN somethink. Whutever; unusuel misteakes?'
txt2=str_extract_all(txt,"[a-zA-Z]+")
txt2
```

```
## [[1]]
## [1] "Speling"   "Errurs"    "IN"        "somethink" "Whutever"  "unusuel"  
## [7] "misteakes"
```

```r
# apply correction to each word
txt3=lapply(txt2[[1]],function(x) case_of(x)(correct(tolower(x))))
txt3
```

```
## [[1]]
## [1] "Spelling"
## 
## [[2]]
## [1] "Errors"
## 
## [[3]]
## [1] "IN"
## 
## [[4]]
## [1] "something"
## 
## [[5]]
## [1] "Whatever"
## 
## [[6]]
## [1] "unusual"
## 
## [[7]]
## [1] "mistakes"
```

```r

# get the correction in the same format or original text sentence
txt4=txt
for(i in 1:length(txt2[[1]])){
  x=txt2[[1]][i]
  txt4=gsub(x,case_of(x)(correct(tolower(x))),txt4)
}
txt4
```

```
## [1] "Spelling Errors IN something. Whatever; unusual mistakes?"
```


## Test of Spell Corrector

Peter Norvig used a set of words with errors for testing. They are in the file [tests1.json][tests1json]. I tried using fromJSON from RJSONIO package but somehow couldn't get it to work. So for the time being I worked with the following file [tests1.txt][tests1txt] (same as json file but with quotes removed) and the code below.


```r
#-----Test based on 270 words with errors-----------------------------------------

# read the list of words with errors: they are in the format 
# target_word: wrong_words (there are cases with greater than 1 possible ways of writing wrong words)
#
tmp=paste(readLines("tests1.txt"),collapse="")
```

```
## Warning: incomplete final line found on 'tests1.txt'
```

```r
# split the list of (target,wrong) pairs
tmp2=strsplit(tmp,",")
# split each (target,wrong) pair based on : separator
tmp3=lapply(tmp2[[1]],function(x) str_trim(str_split(x,":")[[1]]))
# split the list of wrong words using space separator
tmp4=lapply(tmp3,function(x) list(target=x[1],
                                  wrongs=str_split(x[2]," ")[[1]]))

tmp4[1:5]
```

```
## [[1]]
## [[1]]$target
## [1] "access"
## 
## [[1]]$wrongs
## [1] "acess"
## 
## 
## [[2]]
## [[2]]$target
## [1] "accessing"
## 
## [[2]]$wrongs
## [1] "accesing"
## 
## 
## [[3]]
## [[3]]$target
## [1] "accommodation"
## 
## [[3]]$wrongs
## [1] "accomodation" "acommodation" "acomodation" 
## 
## 
## [[4]]
## [[4]]$target
## [1] "account"
## 
## [[4]]$wrongs
## [1] "acount"
## 
## 
## [[5]]
## [[5]]$target
## [1] "address"
## 
## [[5]]$wrongs
## [1] "adress" "adres"
```


This data is used to find percentage of correct instances of spelling correction.


```r
# count total number of words and number of wrong words
ptm <- proc.time()
bad=0
tot=0
for (i in 1:length(tmp4)){
#  print(i)
  nw=length(tmp4[[i]]$wrongs)
  for (j in 1:nw){
      tot=tot+1
      correctword=correct(tmp4[[i]]$wrongs[j])
      if (correctword != tmp4[[i]]$target) {bad=bad+1}
    }
}

# percentage correct
pctcorrect=1-bad/tot
pctcorrect
```

```
## [1] 0.7481
```

```r

proc.time()-ptm
```

```
##    user  system elapsed 
##  528.66    0.04  529.00
```

About 74% of words are correctly corrected for spelling.

## Session Information

This code was written in RStudio (0.98.507).


```r
sessionInfo()
```

```
## R version 3.0.2 (2013-09-25)
## Platform: x86_64-w64-mingw32/x64 (64-bit)
## 
## locale:
## [1] LC_COLLATE=English_United States.1252 
## [2] LC_CTYPE=English_United States.1252   
## [3] LC_MONETARY=English_United States.1252
## [4] LC_NUMERIC=C                          
## [5] LC_TIME=English_United States.1252    
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     
## 
## other attached packages:
## [1] stringr_0.6.2 dplyr_0.2     knitr_1.5    
## 
## loaded via a namespace (and not attached):
## [1] assertthat_0.1 evaluate_0.5.1 formatR_0.10   magrittr_1.0.1
## [5] parallel_3.0.2 Rcpp_0.11.1    tools_3.0.2
```

[tests1json]:https://bitbucket.org/notesofdabbler/blog_notesofdabbler/src/master/norvigSpellCheck/tests1.json
[tests1txt]:https://bitbucket.org/notesofdabbler/blog_notesofdabbler/src/master/norvigSpellCheck/tests1.txt
