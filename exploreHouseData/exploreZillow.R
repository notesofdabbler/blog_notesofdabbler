
# Explore Zillow house listing data

# set working dir
setwd("~/notesofdabbler/Rspace/realEstate")

#load libraries
library(ggplot2)
library(plyr)
library(stringr)
library(reshape2)
library(corrplot)


# global options for ggplot2 graphs
theme_set(theme_bw(20)+theme(strip.background=element_rect(fill="#CCCCFF")))

# load data
load(file="indyHouseListing.Rda")
df=indyHouseListing

# create some more predictors normalized to sqft
df$lotmult=df$lotsqftwunit/df$sqft2
df$numbedsPerSqFt=df$numbeds2/df$sqft2
df$numbathsPerSqFt=df$numbaths2/df$sqft2
df$zestPerSqFt=df$zest2wunit/df$sqft2
df$sellPricePerSqFt=df$sellPrice/df$sqft2

head(df)

# keep only houses with price <= 600000, year >= 2000, area <= 5000 sqft, 
# pricePerSqFt <= 200 and numbaths2 <= 5
df2=subset(df,(price <= 600000) & (yr >= 2000) & 
             (sqft2 <= 5000) & (pricePerSqFt <= 200) & (numbaths2 <= 5))

# count of listing by area
table(df2$area)

# only 10 listings in Zionsville based on this criteria, so exclude
df2=subset(df2,!(area == "Zionsville"))

# compare listing price with zestimate (perSqFt)
ggplot(data=df2,aes(x=pricePerSqFt,y=zestPerSqFt,color=area))+
  geom_point(size=3)+geom_abline(intercept=0,slope=1)

# compare listing price with last sold price
ggplot(data=df2,aes(x=pricePerSqFt,y=sellPricePerSqFt,color=area))+
  geom_point(size=3)+geom_abline(intercept=0,slope=1)


# histogram box plot of price by area
ggplot(data=df2,aes(x=pricePerSqFt))+geom_histogram(aes(y=..density..),binwidth=10)+facet_grid(area~.)
ggplot(data=df2,aes(x=area,y=pricePerSqFt))+geom_boxplot()

# price by year and area
ggplot(data=df2,aes(x=factor(yr),y=pricePerSqFt,fill=area))+geom_boxplot()+xlab("")+
  theme(axis.text.x=element_text(angle=90))

# price by number of beds
ggplot(data=df2,aes(x=factor(numbeds2),y=pricePerSqFt,fill=area))+geom_boxplot()+
  xlab("# beds")

# price by number of baths
ggplot(data=df2,aes(x=factor(numbaths2),y=pricePerSqFt,fill=area))+geom_boxplot()+
  xlab("# baths")

# scatter plot of price with different predictors


pltPred=function(areaPick){
  
df3=subset(df2,df2$area == areaPick)

yvar=c("pricePerSqFt")
xvar=c("yrold","numbeds2","numbaths2","sqft2","doz2","lotsqftwunit",
       "numStories","garSize")


dftest=df3[,c(yvar,xvar)]
dftestm=melt(dftest,id.vars=yvar,variable.name="xvars",value.name="value")
p1=ggplot(data=dftestm,aes(x=value,y=pricePerSqFt))+
  geom_point(size=2)+stat_smooth(method="lm",se=FALSE,size=1.1)+facet_wrap(~xvars,scales="free_x")

# certain predictors normalized with sqFt
xvar2=c("yrold","numbedsPerSqFt","numbathsPerSqFt","sqft2","doz2","lotmult",
        "numStories","garSize")

dftest=df3[,c(yvar,xvar2)]
dftestm=melt(dftest,id.vars=yvar,variable.name="xvars",value.name="value")
p2=ggplot(data=dftestm,aes(x=value,y=pricePerSqFt))+
  geom_point(size=2)+stat_smooth(method="lm",se=FALSE,size=1.1)+facet_wrap(~xvars,scales="free_x")

print(p1)
print(p2)

}

# plot Price per sq feet with different predictors for each area
# Carmel
pltPred("Carmel")
# Greenwood
pltPred("Greenwood")
# Fishers
pltPred("Fishers")

# modeling

yvar=c("pricePerSqFt")
xvar=c("yrold","numbeds2","numbaths2","sqft2","lotsqftwunit",
       "numStories","garSize")
xvar=c("yrold","numbeds2","numbaths2","sqft2",
       "numStories")
xvar=c("yrold","numbedsPerSqFt","numbathsPerSqFt","sqft2","numStories")


areaPick="Carmel"
dfmdl=df2[df2$area == "Carmel",]

summary(dfmdl[,c(yvar,xvar)])

# correlation plot of predictors
corrplot:::corrplot(cor(dfmdl[,xvar]),
                    order="hclust",tl.cex=0.8)

# regression model with all predictors
lmFitAllPred <- lm(pricePerSqFt~.,data=dfmdl[,c(yvar,xvar)])
summary(lmFitAllPred)
predRes=data.frame(obs=dfmdl[,yvar],pred=predict(lmFitAllPred,dfmdl[,xvar]))
ggplot(predRes,aes(x=pred,y=obs))+geom_point()+geom_abline(intercept=0,slope=1)

library(randomForest)
rfModel <- randomForest(dfmdl[,xvar],dfmdl[,yvar],mtry=2,ntree=1000,importance=TRUE)
rfModel
varImpPlot(rfModel)

ypred=predict(rfModel,dfmdl[,xvar])
ggplot(data=data.frame(obs=dfmdl[,yvar],pred=ypred),
       aes(x=pred,y=obs))+geom_point()+geom_abline(intercept=0,slope=1)
sqrt(mean((ypred-dfmdl[,yvar])^2))


